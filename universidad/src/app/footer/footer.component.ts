import { Component } from '@angular/core';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatToolbarModule } from '@angular/material/toolbar';
@Component({
  selector: 'app-footer',
  standalone: true,
  imports: [
    MatToolbarModule,
    CommonModule
  ],
  templateUrl: './footer.component.html',
  styleUrl: './footer.component.css'
})
export class FooterComponent {
  isFlipped: boolean[] = [false];
  flipCard(index: number) {
    this.isFlipped[index] = !this.isFlipped[index]; // Cambiar el estado de volteo de la carta
  }
}
