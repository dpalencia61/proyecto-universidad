import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HomeComponent } from './app/home/home.component'; 
import { MatToolbarModule } from '@angular/material/toolbar';
import { FooterComponent } from './app/footer/footer.component';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [
  ],
  imports: [
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    HomeComponent,
    MatToolbarModule,
    FooterComponent
  ],
  providers: [],
})
export class AppModule { }
